#include "hapNotes.h"
#include "HapTracks.h"

int MajorVersion = 1;
int MinorVersion = 1;

int TrackAdvanceButtonPin = 13;  // Which pin is attached to button to advance the track number

int trackNdx = 0;
int previousTrackAdvanceButtonState = LOW;
event_t *event_a =  Track_a[0];;
unsigned int eventPos = 0; // Position in current track
unsigned long baseTime = 0;

void setup() {

  int pwmDutyCycle = 50;

  Serial.println("====================");
  Serial.println("  Haptic Player");
  Serial.print("    Version ");
  Serial.print(MajorVersion);
  Serial.print(".");
  Serial.print(MinorVersion);
  Serial.println("");
  Serial.println("====================");

  pinMode(TrackAdvanceButtonPin,INPUT);

  for(int i=0; i< PIN_COUNT; i++) {
    
    pinMode(CtrlPinMap[i],OUTPUT);
//    pinMode(PwmPinMap[i],OUTPUT);
//    analogWrite(PwmPinMap[i],pwmDutyCycle);
  }

    Serial.begin(9600);

    trackNdx = 0;
    event_a =  Track_a[trackNdx];;
    eventPos = 0; // Position in current track
    baseTime = millis();
}

// Idea is to play the current track over and over with no pause... until the track advance button is
// pressed, in which case advance to the next track (and cycle back to zero after all tracks)
void loop()
{

    // faster processing if we hold the loop here
//    while(1)
    {
        int trackAdvanceButtonState = digitalRead(TrackAdvanceButtonPin);
//        trackAdvanceButtonState = LOW;
        // Take action only if button state changed high to low or low to high
        if (trackAdvanceButtonState != previousTrackAdvanceButtonState)
        {
            Serial.println("BUTTON STATE CHANGE");
         
            if (trackAdvanceButtonState == HIGH)
            {
                trackNdx = (trackNdx+1)%TRACK_COUNT;
                event_a = Track_a[trackNdx];
                eventPos = 0; // start at start of this track
                baseTime = millis();

                Serial.print("======== Switch to Track ");
                Serial.print(trackNdx);
                Serial.println(" ========");
            }
            previousTrackAdvanceButtonState = trackAdvanceButtonState;
        }

        int currentTime = millis() - baseTime;
//        Serial.print("Current Time: ");
//        Serial.println(currentTime);
        
//        Serial.print("Target Time: ");
//        Serial.println(event_a[eventPos].time);
        
        if (currentTime >= event_a[eventPos].time)
        {
            Serial.print("Track ");
            Serial.print(trackNdx);
            Serial.print("  ");
            Serial.print(eventPos);
            Serial.print("  ");
          
            switch(event_a[eventPos].opcode) {
            case OP_ANALOG_WRITE:
                analogWrite(event_a[eventPos].arg0,event_a[eventPos].arg1);
                break;
                  
            case OP_DIGITAL_WRITE:
                digitalWrite(event_a[eventPos].arg0,event_a[eventPos].arg1); 
                
                Serial.print("  write( ");
                Serial.print(event_a[eventPos].arg0);
                Serial.print(",");
                Serial.print(event_a[eventPos].arg1);
                Serial.println(" )");
                break;
                  
            case OP_END_OF_TRACK:
              
                 // restart track
                 eventPos = -1; // Gets advanced to zero before next loop
                 baseTime = millis();
                 Serial.println("### End Track###");
                 break;
            } // opcode switch
            
            eventPos++; // We've executed this event, on to the next one
            
            
        }
        else
        {
           // Serial.println("NOT YET");
           //   delay(events[ndx].time - now - 1); // TBD: should we be doing something else here?
        }
      
    } // End while (true)
}
