//#include <Volume3.h>

//             Haptor Index    Pin
//                  0          3
//                  1          5
//                  2          6
//                  3          9
//                  4          10
//                  5          11
#define PIN_COUNT 5
unsigned char CtrlPinMap[PIN_COUNT]    = {15, 2, 0 , 4, 5};
//unsigned char PwmPinMap[PIN_COUNT] = {3, 5, 6, 9, 10, 11};

typedef struct {
   unsigned long time;
   unsigned char opcode;
   unsigned char arg0;
   unsigned char arg1;
} event_t;

#define OP_END_OF_TRACK 0
#define OP_ANALOG_WRITE 1
#define OP_DIGITAL_WRITE 2

void PlayTrack(event_t events[])
{
    unsigned long startTime = millis();
    unsigned int ndx = 0;
    while (events[ndx].opcode != OP_END_OF_TRACK)
    {
        unsigned long now = millis() - startTime;
//        Serial.print("now");
//        Serial.println(now);
        if (now >= events[ndx].time)
        {
            switch(events[ndx].opcode) {
            case OP_ANALOG_WRITE:    analogWrite(events[ndx].arg0,events[ndx].arg1); break;
            case OP_DIGITAL_WRITE:   digitalWrite(events[ndx].arg0,events[ndx].arg1); 
              Serial.write(48+events[ndx].arg0);
              if (events[ndx].arg1 == LOW)
              {
                Serial.write(" ON ");
              }
              else
              {
                Serial.write(" OFF ");
              }
            
            break;
            case OP_END_OF_TRACK:
            break;
            }
            ndx++; // We've executed this event, on to the next one
        }
      else {
       // Serial.println("NOT YET");
       //   delay(events[ndx].time - now - 1); // TBD: should we be doing something else here?
      }
    }
}

//int mapHardware(int *pLevel){
//  int pin = 0;
//  if(jounce == 1){
//    pin = jounce1;
//    *pLevel = map(*pLevel, 0, 255, 0, 168);
//  }else if(jounce == 2){
//    pin = jounce2;
//    *pLevel = map(*pLevel, 0, 255, 0, 168);
//  }
//  return  pin;
//}

//void Rectangle(int jounce, int frequency, int level, int duration){
//  int pin = mapHardware(jounce, &level);
//  int start_time = millis();
//  vol.tone(pin, frequency, level);
//  while(start_time - millis() < duration){
//    delay(1);
//  }
//  digitalWrite(pin, LOW);
//}

//void Ramp(int jounce, int frequency, int level, int duration){
//  int pin = mapHardware(jounce, &level);
//}
//
//void AnalogWrite(int pin,int level){
//  int pin = PinMap[haptorIndex];
//  int level = map(rawLevel, 0, 255, 0, 168);
//  analogWrite(pin, level);
//  delay(50);
//  digitalWrite(pin, LOW);
//}
//
//void Tap(int haptorIndex, int rawLevel){
//  int pin = PinMap[haptorIndex];
//  int level = map(rawLevel, 0, 255, 0, 168);
//  analogWrite(pin, level);
//  delay(50);
//  digitalWrite(pin, LOW);
//}
//
//void Pause(int duration){
//  delay(duration);
//}

