############################################################################
# HapTrack.py
#
# Defines the file & memory layout of a HapTrack.
############################################################################
from Haptor import *

##   0000 HT3\0
##   0004 <command code> (1 byte)

# for TouchEvent command code:
##   Touch-Point-Index, Start-Time, FuncCode, [args: varies per FuncCode]

HapTrackMajorVersion = 1
HapTrackMinorVersion = 0

HapFunc_Undefined = 0        # Not a function
HapFunc_Tap = 1              # Args: 1 byte level, 2 byte duration, 1 byte freq
HapFunc_End_of_Events = 255  # No more events


#############################################################################
class HapEvent(object):
    def __init__(self,time,func,haptor,argList):
        self.time = time
        self.functionCode = func
        self.haptor = haptor
        self.args = argList

    #############################################################################
    def GetPinEvents(self):

        pinEvents = []

        if self.functionCode == HapFunc_Tap:
            (duration,level) = self.args
            # Generate a pin ON and pin OFF event after specified duration
#            pinEvents.append("{%6d, %20s, PinMap[%2d],%d}," % (self.time,"OP_ANALOG_WRITE",self.haptor.id,self.haptor.GetScaledLevel(level)))
            pinEvents.append("{%6d, %20s, CtrlPinMap[%2d],HIGH}," % (self.time,"OP_DIGITAL_WRITE",self.haptor.id))
            pinEvents.append("{%6d, %20s, CtrlPinMap[%2d],LOW}," % (self.time+duration,"OP_DIGITAL_WRITE",self.haptor.id))
        elif self.functionCode == HapFunc_End_of_Events:
            pinEvents.append("{%6d, %20s, 0,0}" % (self.time, "OP_END_OF_TRACK"))

        else:
            print("Error: GetPinEvents() unhandled functionCode[%d]"%self.functionCode)

        return pinEvents

#############################################################################
class HapTrack(object):

    #############################################################################
    ############################################################################
    def __init__(self,shortName,description,author,trackMajorRev,trackMinorRev):
        self.trackName = shortName # Must be suitable as a function name
        self.trackDescription = description
        self.trackAuthor = author
        self.trackMajorRevision = trackMajorRev
        self.trackMinorRevision = trackMinorRev

        self.events = [] # Array of HapEvents

    #############################################################################
    def RenderC(self,file):

        file.write("//  Track Name: %s\n" % self.trackName )
        file.write("// Description: %s\n" % self.trackDescription )
        file.write("//      Author: %s\n" % self.trackAuthor)
        file.write("//    Revision: %d.%d\n" %(self.trackMajorRevision,self.trackMinorRevision))

        file.write("event_t Track_%s[] = {\n" % self.trackName)

        pinEvents = []

        for event in self.events:
            pinEvents.extend(event.GetPinEvents())

        pinEvents.sort() # Our pin event lines are text with time on lhs, so alpha sort works

        for pevent in pinEvents:
            file.write(pevent + "\n")

        file.write("}; // End Track %s\n" % self.trackName)

    #############################################################################
    def DebugDump(self):
        print "  Track Name: "+ self.trackName
        print " Description: "+ self.trackDescription
        print "      Author: "+ self.trackAuthor
        print "   Track Rev: %d.%d" % (self.trackMajorRevision,self.trackMinorRevision)
        print ""

    #############################################################################
    def WriteTap(self, startTime, haptor, tapDuration, tapLevel):

        event = HapEvent(startTime,HapFunc_Tap,haptor,[tapDuration,tapLevel])
        self.events.append(event)

    #############################################################################
    def WriteEndTrack(self, startTime):

        event = HapEvent(startTime,HapFunc_End_of_Events,0,[])
        self.events.append(event)

