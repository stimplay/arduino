############################################################################
# Haptrix
#
# Allows programatic creation and editing of HapTracks
# - Matt Lavoie
#   4/11/2018
#   7/29/2018

############################################################################
import getopt
import marshal
import sys
import random
import os
from HapTrack import *

###############################################################################
###############################################################################
class CHaptrixException(Exception):
    "An exception encountered in Haptrix"

###############################################################################
###############################################################################
# Global variables
###############################################################################
###############################################################################

class CHaptrixApp(object):

    ###############################################################################
    def __init__(self):
        
        self.appVersion = '1.20'
        self.options = {}           # Dictionary of program options
        self.tracks = []            # list of generated tracks
        self.haptors = []           # list of haptor (touch devices)

    ###############################################################################
    def ConfigureHaptors(self):

        for i in range(0,4):
            haptor = HaptorDevice(i,HaptorDeviceType_ERM,"Blue Pager Vibrator",0,168)
            self.haptors.append(haptor)

    ###############################################################################
    def GenFastTapSeries(self):

        trackTitle = 'FastTapSeries'
        trackDescription = 'Hit all points in sequence, 1/2 second each, 50 ms between, 1 seconds pause each cycle'

        tapDuration = 500         # in milliseconds
        tapLevel = 255            # max tap strength
        pauseBetweenTaps = 50     # in milliseconds
        pauseAfterSeries = 1000   # two second pause between sets

        touchPoints = range(len(self.haptors)) # use all haptors

        genTrack = HapTrack(trackTitle,trackDescription,'Matt', 1, 0)

        time = 0  # in milliseconds from start of track
        for pt in touchPoints:

            genTrack.WriteTap(time, self.haptors[pt], tapDuration, tapLevel)
            time += tapDuration
            time += pauseBetweenTaps
        time += pauseAfterSeries

        genTrack.WriteEndTrack(time)   # Save our newly generated track to our list of tracks
        self.tracks.append(genTrack)

    ###############################################################################

    def GenOverlappingSequence(self):

        trackTitle = 'OverlappingSequence'
        trackDescription = 'Hit all points, with overlapping start times'

        tapDuration = 1000        # in milliseconds
        tapLevel = 255            # max tap strength

        touchPoints = range(len(self.haptors)) # use all haptors

        genTrack = HapTrack(trackTitle,trackDescription,'Author', 1, 0)

        time = 0  # in milliseconds from start of track

        # 1  TTTTTTTT
        # 2    TTTTTTTT
        # 3      TTTTTTTT
        # 4        TTTTTTTT

        for pt in touchPoints:
            genTrack.WriteTap(time, self.haptors[pt], tapDuration, tapLevel)
            time += tapDuration / (len(touchPoints))

        time += tapDuration
        # Save our newly generated track to our list of tracks
        genTrack.WriteEndTrack(time)
        self.tracks.append(genTrack)

    ###############################################################################
    def GenPairSequence(self):

        trackTitle = 'PairsSequence'
        trackDescription = 'Pairs of 2 Sequence'

        tapDuration = 1000  # in milliseconds
        tapLevel = 255  # max tap strength

        touchPoints = range(len(self.haptors))  # use all haptors

        genTrack = HapTrack(trackTitle, trackDescription, 'Author', 1, 0)

        time = 0  # in milliseconds from start of track

        for i in range(0,len(touchPoints)-1,2):
            genTrack.WriteTap(time, self.haptors[  i], tapDuration, tapLevel)
            genTrack.WriteTap(time, self.haptors[i+1], tapDuration, tapLevel)
            time += tapDuration

        time += 1
        # Save our newly generated track to our list of tracks
        genTrack.WriteEndTrack(time)
        self.tracks.append(genTrack)

    ###############################################################################
    def GenAllOnOff(self):

        trackTitle = 'AllOnOff'
        trackDescription = 'Hit all points, 1 second on, 1 second off'

        tapDuration = 1000        # in milliseconds
        tapLevel = 255            # max tap strength

        touchPoints = range(len(self.haptors)) # use all haptors

        genTrack = HapTrack(trackTitle,trackDescription,'Author', 1, 0)

        time = 0  # in milliseconds from start of track

        for pt in touchPoints:
            genTrack.WriteTap(time, self.haptors[pt], tapDuration, tapLevel)
            time += tapDuration

        time += 1

        # End of track time is 1 second in the future, so played in a loop... we get
        # a pause between all on, all off
        genTrack.WriteEndTrack(time)
        self.tracks.append(genTrack)

    def GenerateTracks(self):
        self.GenFastTapSeries()
        self.GenOverlappingSequence()
        self.GenPairSequence()
        self.GenAllOnOff()

    #############################################################################
    def Gen_C_Code(self):

        file = open("HapTracks.h", "w")

        file.write("// Generated by Haptrix %s\n" % (self.appVersion))
        file.write("// Copyright 2018\n")
        file.write("\n")
        file.write("#define TRACK_COUNT %d\n" % len(self.tracks))
        file.write("\n")

        for track in self.tracks:
            track.RenderC(file)
            file.write("\n")

        file.write("event_t *Track_a[TRACK_COUNT] = {\n")

        for track in self.tracks:
            file.write("  Track_" + track.trackName + ",\n")

        file.write("};\n")

        file.close()

    ###############################################################################
    def Start(self):

        self.LoadOptions()  # Loads options from disk

        self.SetOptionDefaults()

        if self.options['verbose'] > 0:
            App.Log("      Verbose Level: %d" % (self.options['verbose']))

        self.ConfigureHaptors()
        self.GenerateTracks()
        self.Gen_C_Code()

        for track in self.tracks:
            track.DebugDump()

        self.SaveOptions()  # In case user changed any (when GUI active later)

    ###############################################################################
    def Usage(self):
        print "Options:"
        print "  --help                  This page"
        print "  --verbose=n             Set verbosity level (0=off, 3=max)"

    ###############################################################################
    def ParseCommandLine(self):
        try:
            opts, args = getopt.getopt(sys.argv[1:], "hv:", ["help", "verbose=", ])
        except getopt.GetoptError, err:
            print str(err)  # will print something like "option -a not recognized"
            self.Usage()
            sys.exit(2)

        for optStr, val in opts:
            if optStr in ("-v", "--verbose"):
                self.options['verbose'] = int(val)
            elif optStr in ("-h", "--help"):
                usage()
                sys.exit()
            else:
                App.Log("unrecognized option: %s" % optStr)
                sys.exit(2)

    ###############################################################################
    def SetOptionDefaults(self):
        
        if not 'verbose'                   in self.options:                   self.options['verbose'] = 0

           
    ###############################################################################
    def LoadOptions(self):
        # Loading this way allows us to give cmdline precedence over options saved to 
        # our config file.  And then anything that still isn't set... we set to defaults 
        if os.path.exists('HaptrixConfig.bin'):
            fp = open('HaptrixConfig.bin','rb')
            savedOptions = marshal.load(fp)
            fp.close()
            for opt in savedOptions.iterkeys():
                if not opt in self.options:
                    self.options[opt] = savedOptions[opt]
        self.SetOptionDefaults() # Sets any settings that were not in the config file or cmdline

    ###############################################################################
    def SaveOptions(self):

        # Write the data so we have it for next time
        fp = open('HaptrixConfig.bin','wb')
        marshal.dump(self.options,fp)
        fp.close()

    ###############################################################################
    def Log(self,msg):
        print msg
           
###############################################################################
###############################################################################

# Global variable to hold our entire app state
App = CHaptrixApp()

if __name__ == "__main__":
    # Import Psyco if available
    # Psycho is a module that increases the performance of python code
    try:
        import psyco
        psyco.full()
#        print "Psycho acceleration enabled."
    except ImportError:
        pass
    
    App.Start()
