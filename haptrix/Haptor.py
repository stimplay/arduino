
HaptorDeviceType_Undefined = 0  # Undefined device
HaptorDeviceType_ERM       = 1  # Eccentric Rotating Mass (ERM) Vibrator
#############################################################################
class HaptorDevice(object):
    def __init__(self,haptorId,type,name,minLevel,maxLevel):
        self.id = haptorId
        self.type = type
        self.name = name
        self.minLevel = minLevel
        self.maxLevel = maxLevel
        self.levelStep = (self.maxLevel-self.minLevel)/255.0

    def GetScaledLevel(self,rawLevel):
        ''' rawLevel comes in as [0,255], we remap to [minLevel,maxLevel]'''
        return int(self.levelStep * rawLevel)